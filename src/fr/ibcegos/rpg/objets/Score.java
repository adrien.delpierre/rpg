package fr.ibcegos.rpg.objets;

public class Score {

	private int nombreMonstreFacileTue;
	private int nombreMonstreDifficileTue;
	
	public Score() {
	}

	public int getNombrePoint() {
		return nombreMonstreFacileTue + nombreMonstreDifficileTue * 2;
	}

	public int getNombreMonstreFacileTue() {
		return nombreMonstreFacileTue;
	}

	public void setNombreMonstreFacileTue(int nombreMonstreFacileTue) {
		this.nombreMonstreFacileTue = nombreMonstreFacileTue;
	}

	public int getNombreMonstreDifficileTue() {
		return nombreMonstreDifficileTue;
	}

	public void setNombreMonstreDifficileTue(int nombreMonstreDifficileTue) {
		this.nombreMonstreDifficileTue = nombreMonstreDifficileTue;
	}
	
	public void incrementerCompteurFacile() {
		nombreMonstreFacileTue++;
	}

	public void incrementerCompteurDifficile() {
		nombreMonstreDifficileTue++;
	}

	@Override
	public String toString() {
		return "Score [getNombrePoint()=" + getNombrePoint() + ", getNombreMonstreFacileTue()="
				+ getNombreMonstreFacileTue() + ", getNombreMonstreDifficileTue()=" + getNombreMonstreDifficileTue()
				+ "]";
	}
	

}
