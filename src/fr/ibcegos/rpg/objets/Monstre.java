package fr.ibcegos.rpg.objets;

public abstract class Monstre {
	
	private boolean vivant;
	private final int ATTAQUE_DE_BASE = 10;
	
	public Monstre() {
		this.vivant = true;
	}

	public boolean isVivant() {
		return vivant;
	}
	
	public void subirDegat() {
		this.vivant = false;
	}
	
	public void attaquer(Joueur joueur) {
		De de = new De();
		int jetMonstre = de.genererNombreAleaEntre1et6();
		int jetJoueur = de.genererNombreAleaEntre1et6();
		if(jetMonstre > jetJoueur) {
			joueur.subirDegat(ATTAQUE_DE_BASE);
		}
	}
	

}
