package fr.ibcegos.rpg.objets;

public class Joueur {
	
	private int pointDeVie;
	private Score score;
	
	public Joueur() {
		this(150); // chainage
	}

	public Joueur(int pointDeVie) {
		setPointDeVie(pointDeVie);
		this.score = new Score();
	}
	
	public int getPointDeVie() {
		return pointDeVie;
	}

	public void setPointDeVie(int pointDeVie) {
		if(pointDeVie < 0) {
			this.pointDeVie = 0;
		} else {
			this.pointDeVie = pointDeVie;
		}
	}

	public boolean isVivant() {
		if(this.pointDeVie > 0) {
			return true;
		}
		return false;
	}
	
	public Score getScore() {
		return score;
	}

	public void subirDegat(int montantDegat) {
		De de = new De();
		int jetJoueur = de.genererNombreAleaEntre1et6();
		if(jetJoueur > 2) { // bouclier
			this.setPointDeVie(this.pointDeVie - montantDegat);
		}
	}
	
	public void attaquer(Monstre monstre) {
		De de = new De();
		int jetMonstre = de.genererNombreAleaEntre1et6();
		int jetJoueur = de.genererNombreAleaEntre1et6();
		if(jetJoueur >= jetMonstre) {
			monstre.subirDegat();
		}
	}
	
	public void augmenterScore(Monstre monstre) {
		if(monstre instanceof MonstreFacile) {
			score.incrementerCompteurFacile();
		} else {
			score.incrementerCompteurDifficile();
		}
		
	}

	@Override
	public String toString() {
		return "Joueur [getPointDeVie()=" + getPointDeVie() + ", isVivant()=" + isVivant() + "]";
	}


	

}
