package fr.ibcegos.rpg.objets;

public class De {
	
	
	public int genererNombreAleaEntre1et6() {
		return genererNombreAlea(1,6); // chainage
	}
	
	public int genererNombreAlea(int min, int max) {
		return min + (int)(Math.random() * ((max - min) + 1));
	}
	
	public Monstre genererMonstreAlea() {
		int lancerDe = genererNombreAlea(1,2);
		if(lancerDe == 1) {
			return new MonstreFacile();
		}
		return new MonstreDifficile();
	}
	

}
