package fr.ibcegos.rpg;

import java.util.Scanner;

import fr.ibcegos.rpg.objets.De;
import fr.ibcegos.rpg.objets.Joueur;
import fr.ibcegos.rpg.objets.Monstre;
import fr.ibcegos.rpg.objets.MonstreFacile;

public class Launcher {
	
	public static void main(String[] args) {
		
		int pointDeVie = affichageMenuJeu();
		
		De de = new De();
		Monstre monstre = de.genererMonstreAlea();
		Joueur joueur = new Joueur(pointDeVie);

		while(joueur.isVivant()) {
			if(!monstre.isVivant()) {
				monstre = de.genererMonstreAlea();
			}
			afficherTypeDeMonstre(monstre);
			joueur.attaquer(monstre);
			affichageSurvieMonstre(monstre);
			if(monstre.isVivant()) {
				affichageAttaqueMonstre();
				monstre.attaquer(joueur);
			} else {
				joueur.augmenterScore(monstre);
			}
		}
		affichageDeFin(joueur);
	}

	private static void affichageDeFin(Joueur joueur) {
		System.out.println("Snif, vous etes mort ......");
		System.out.print("Bravo, vous avez tu� " + joueur.getScore().getNombreMonstreFacileTue() + " monstres faciles");
		System.out.println(" et " + joueur.getScore().getNombreMonstreDifficileTue() + " monstres difficiles.");
		System.out.println("Vous avez " + joueur.getScore().getNombrePoint() + " points.");
	}

	private static int affichageMenuJeu() {
		System.out.println("Bienvenue sur le RPG version 1.0.0");
		System.out.println("Merci de saisir le nombre de point de vie du joueur");
		Scanner sc = new Scanner(System.in);
		int pointDeVie = sc.nextInt();
		return pointDeVie;
	}

	private static void affichageAttaqueMonstre() {
		System.out.println("Le monstre attaque le joueur");
	}

	private static void affichageSurvieMonstre(Monstre monstre) {
		if(monstre.isVivant()) {
			System.out.println("Le monstre a surv�cut � l'attaque du joueur");
		} else {
			System.out.println("Le monstre est mort");
		}
	}

	private static void afficherTypeDeMonstre(Monstre monstre) {
		System.out.print("Le joueur affronte un monstre ");
		if(monstre instanceof MonstreFacile) {
			System.out.println("facile");
		} else {
			System.out.println("difficile");
		}
	}

}
